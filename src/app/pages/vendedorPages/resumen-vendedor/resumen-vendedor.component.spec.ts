import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenVendedorComponent } from './resumen-vendedor.component';

describe('ResumenVendedorComponent', () => {
  let component: ResumenVendedorComponent;
  let fixture: ComponentFixture<ResumenVendedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenVendedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenVendedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
