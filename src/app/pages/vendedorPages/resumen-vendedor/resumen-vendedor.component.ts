import { Component, OnInit } from '@angular/core';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';

@Component({
  selector: 'app-resumen-vendedor',
  templateUrl: './resumen-vendedor.component.html',
  styleUrls: ['./resumen-vendedor.component.scss']
})
export class ResumenVendedorComponent implements OnInit {
  vendedor: any;
  option = {
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      data: ['mi venta', 'mi meta']
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: ['Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        formatter: '{value}'
      }
    },
    series: [
      {
        name: 'mi venta',
        type: 'line',
        data: [20525647,44312583, 35901853, 36288728, 45236623,45836322 ],
      },
      {
        name: 'mi meta',
        type: 'line',
        data: [33000000, 46300000, 49000000, 47300000 , 55000000,51000000 ],

      }
    ]
  };

  option2 = {
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      data: ['mi venta', 'mi meta']
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: ['25-11-19', '02-12-19', '09-12-19', '16-12-19', '23-12-19', '30-12-19']
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        formatter: '{value}'
      }
    },
    series: [
      {
        name: 'mi venta',
        type: 'line',
        data: [772525,7725225, 7725225, 7725225, 7725225,0 ],
      },
      {
        name: 'mi meta',
        type: 'line',
        data: [8551279, 6583171, 49000000, 11783575 , 7527584,976909 ],

      }
    ]
  };
  constructor(private dataLocal: DataLocalService) { }

  async ngOnInit() {
    this.vendedor = JSON.parse(await this.dataLocal.obtenerItem('resumenVt'));
    console.log('vendedor', this.vendedor);
  }

}
