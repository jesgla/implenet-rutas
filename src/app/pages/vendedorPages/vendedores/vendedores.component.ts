import { Component, OnInit } from '@angular/core';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.component.html',
  styleUrls: ['./vendedores.component.scss']
})
export class VendedoresComponent implements OnInit {
  lstVendedores: any;

  constructor(private vendedorService : VendedorService, private dataLocal : DataLocalService,private router : Router) { }

  async ngOnInit() {
    this.lstVendedores = await this.vendedorService.obtenerVT();
    this.lstVendedores.sort((a,b)=>a.nombreCompleto<b.nombreCompleto?-1:null)
    for (let index = 0; index < this.lstVendedores.length; index++) {
      let vt = this.lstVendedores[index];
      let datos = await this.vendedorService.obtenerObjetivos(vt);
      vt.metaMes = await datos[0].Meta;

      vt.metaAcumulada = datos[2].MetaAcumulada;
      vt.ventaAcumulada = datos[0].Venta;
      vt.ventaAcumuladaProcentaje = vt.ventaAcumulada / vt.metaAcumulada;
      vt.porcentajeMetaMesGrafico = Math.round(vt.ventaAcumuladaProcentaje * 100)>0?Math.round(vt.ventaAcumuladaProcentaje * 100):0;
  
  
      vt.ventaAcumulada = Number((vt.ventaAcumulada ).toFixed(1));
      vt.metaAcumulada = Number((vt.metaAcumulada ).toFixed(1));
  
      vt.mentaHoy = datos[2].Meta;
      vt.ventaDia = datos[2].Venta;
      vt.ventaDiaPorcentaje = vt.ventaDia / vt.mentaHoy;
      vt.mentaHoy = Number((vt.mentaHoy ).toFixed(1));
      vt.ventaDia = Number((vt.ventaDia ).toFixed(1));
      console.log('vt',vt);
      
    }
    this.lstVendedores=this.lstVendedores.filter(vt=>vt.metaMes>0)
    console.log('vt',this.lstVendedores);

  }
  async resumenVendedor(vendedor){
    await this.dataLocal.guardarItem('resumenVt', JSON.stringify(vendedor));
    this.router.navigate(['resumenVendedor'])
  }
}
