import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbModal, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Vendedor } from 'src/app/interfaces/vendedor';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { VendedorService } from 'src/app/services/vendedor/vendedor.service';
import { ModalService } from 'src/app/services/modal/modal.service';
import { DataLocalService } from 'src/app/services/dataLocal/data-local.service';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-llamados',
  templateUrl: './llamados.component.html',
  styleUrls: ['./llamados.component.scss']
})
export class LlamadosComponent implements OnInit {

  lstVendedores: Vendedor[];
  diaHoy: number;
  textoBuscar: string = '';
  id: boolean = false;
  nombre: boolean = false;
  clientes: boolean = false;
  contactados: boolean = false;
  contactadosFallidos: boolean = false;
  minLlamadas: boolean = false;
  gerente: boolean = false;
  codTienda: boolean = false;
  tienda: boolean = false;
  public innerWidth: any;
  totalVentas: number;
  totalLlamadasN: number;
  totalLlamadasC: number;
  tipoFiltro: string;
  arrayBuffer: any;
  file: File;
  lstFiltrada: any[];
  lst: any;
  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;
  formFechas: FormGroup;
  constructor(
    private formGroup: FormBuilder,
    private vendedorService: VendedorService,
    private modalService: NgbModal,
    private dataModal: ModalService,
    private dataLocal: DataLocalService,
    private calendar: NgbCalendar, public formatter: NgbDateParserFormatter,
    private router: Router) {
    this.diaHoy = new Date().getDate();
    this.tipoFiltro = 'diario';
    this.fromDate = calendar.getToday();

    this.toDate = calendar.getNext(calendar.getToday(), 'd', 0);
    this.formFechas = this.formGroup.group({
      fechaInicio: new FormControl('', Validators.required),
      fechaTermino: new FormControl('', Validators.required)
    })
  }
  onResize(event) {

    this.innerWidth = event.target.innerWidth;
  }

  async ngOnInit() {
    this.innerWidth = window.innerWidth;
    let fechaInicio = new Date().setHours(0, 0, 0, 0);
    let fechaTermino = new Date().setHours(23, 59, 59, 59);

    this.lst = await this.vendedorService.obtenerVendores(fechaInicio, fechaTermino);
   
    this.setearDatos(fechaInicio, fechaTermino);

  }
  async setearDatos(fechaInicio, fechaTermino) {
    let revisar=[]
    this.totalVentas = 0;
    let sumaCont = 0;
    let sumaNoCont = 0;
    let totalVenta = 0;
    for (let index = 0; index < this.lst.length; index++) {
      const vendedor = this.lst[index];
      let cont = 0;
      let contF = 0;
      let segundo = 0;
      totalVenta = 0;

      let clientes = vendedor.clientes;

      for (let index = 0; index < clientes.length; index++) {

        const cliente = clientes[index];
        let filtroC =  cliente.registros.filter(llamadaT=>fechaInicio < Number(llamadaT.idllamada) && Number(llamadaT.idllamada) <= fechaTermino && llamadaT.segundos > 0);
        let filtroN =  cliente.registros.filter(llamadaT=>fechaInicio < Number(llamadaT.idllamada) && Number(llamadaT.idllamada) <= fechaTermino && llamadaT.segundos == 0);
        filtroC.length>0 ?cont += 1:null;
        filtroN.length>0 ?contF += 1:null;
      
        for (let indice = 0; indice < cliente.registros.length; indice++) {

          const llamada = cliente.registros[indice];
          let fechaLlamada = Number(llamada.idllamada);
          let total = 0;
          
         

          if (fechaInicio < fechaLlamada && fechaLlamada <= fechaTermino && llamada.segundos > 0) {
            if (llamada.segundos > 39) {

              let ventas = vendedor.ventas;
              ventas.filter(venta => {
                let fechab = new Date(venta.Fecha).getTime();
                let valor = 24 * 60 * 60 * 1000 * 7;
  
  
                let fechaI = new Date(fechaLlamada).setHours(0, 0, 0, 0)
                let semana = fechaI + valor;
                let fechaT = new Date(semana).setHours(23, 59, 59, 59);
                if (venta.cliente == cliente.rut && fechaInicio < fechaLlamada && fechaLlamada <= fechaTermino && fechaI < fechab && fechab <= fechaT && vendedor.id === venta.codEmpleado && venta.tipoTransaccion !== "NCE") {
                  revisar.push({
                    subgerente: vendedor.subgerente,
                    tienda: vendedor.tienda,
                    id: vendedor.id,
                    index:index,
                    nombreV: vendedor.nombre,
                    rut: cliente.rut,
                    nombreC: cliente.nombre,
                    fechaL: fechaLlamada,
                    fechaDoc: venta.Fecha,
                    doc: venta.numeroDocumento,
                    sku: venta.SKU,
                    venta: Math.round(venta.VentaUnit),
                    cantidad: Math.round(venta.cantidad),
                    total: Math.round(venta.VentaUnit * venta.cantidad),
                    segundos: llamada.segundos
                  });
                  
                }
              })


            }
            //cont += 1;
            segundo += llamada.segundos;

          } else if (fechaInicio < fechaLlamada && fechaLlamada <= fechaTermino && llamada.segundos === 0) {


            //contF += 1
          }
         
        }


      }
      
      vendedor.totalVenta=0;
      vendedor.contactados = cont;
      vendedor.contactadosFallidos = contF;
      vendedor.minLlamadas = segundo;
      sumaCont += cont;
      sumaNoCont += contF;

    }
    for (let index = 0; index < revisar.length - 1; index++) {
      const venta = revisar[index];
      let existe = false;
      for (let i = index + 1; i < revisar.length; i++) {
        const copiaVenta = revisar[i];
        let fechab =new Date(copiaVenta.fechaDoc).getTime();
        let valor = 24 * 60 * 60 * 1000 * 7;


        let fechaI = new Date(venta.fechaL).setHours(0, 0, 0, 0)
        let semana = fechaI + valor;
        let fechaT = new Date(semana).setHours(23, 59, 59, 59);

        if (fechaI < fechab && fechab < fechaT  && venta.doc == copiaVenta.doc && venta.sku == copiaVenta.sku ) {
          
          existe=true
        }
      }
      if(!existe){
    
          this.lst.filter(vendedor=>{
            if(vendedor.id ==venta.id){
              vendedor.totalVenta+=venta.total
             

            }
          })
          this.totalVentas+= venta.total
      }
    }
    this.lstVendedores = this.lst;

    this.totalLlamadasC = Math.round(sumaCont);
    this.totalLlamadasN = Math.round(sumaNoCont);
  }

  async filtrarData() {

    this.lstVendedores = undefined;

    let { fechaInicio, fechaTermino } = this.formFechas.value;

    let fechas = this.rangoFecha(fechaInicio, fechaTermino)
 
    this.lst = await this.vendedorService.obtenerVendores(fechas.fechaInicio, fechas.fechaTermino);

    this.setearDatos(fechas.fechaInicio, fechas.fechaTermino);
  }
  rangoFecha(fechaInicio, fechaTermino) {
    let valor = 24 * 60 * 60 * 1000 * 7;


    fechaInicio = new Date(new Date(fechaInicio).getTime() + 24 * 60 * 60 * 1000);
    fechaInicio = fechaInicio.setHours(0, 0, 0, 0)
    fechaTermino = new Date(new Date(fechaTermino).getTime() + 24 * 60 * 60 * 1000).setHours(0, 0, 0, 0);

    let semana = fechaTermino + valor;

    fechaTermino = new Date(semana).setHours(23, 59, 59, 59);
  

    let fechas = {
      fechaInicio: fechaInicio, fechaTermino: fechaTermino
    }
    return fechas
  }
 

  incomingfile(event) {
    this.file = event.target.files[0];
  }
  onFileChange() {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      let json = XLSX.utils.sheet_to_json(worksheet, { raw: true })

      //this.insertarProductos(json);

    }
    fileReader.readAsArrayBuffer(this.file);
  }

  detalleLlamadas(vendedor) {
    let { fechaInicio, fechaTermino } = this.formFechas.value;

    let fechas = this.rangoFecha(fechaInicio, fechaTermino)
    let objeto ={
      vendedor:vendedor,
      inicio:fechas.fechaInicio,
      termino:fechas.fechaTermino,
    }
    this.dataLocal.guardarItem('vendedor', objeto);
    this.router.navigate(['/llamadas']);
  }

  exportAsXLSX(): void {
    let excel = [];
    excel.push(["Listado vendedores"]);
    excel.push([]);
    excel.push(['Gerente zona', 'Cod tienda', 'tienda', 'id', 'Vendedor', 'Asignados', 'Contactados', 'Intentos fallidos', 'Total Venta', 'Total duracion llamadas']);
    for (let index = 0; index < this.lstVendedores.length; index++) {
      const vendedor = this.lstVendedores[index];
      excel.push([vendedor.subgerente, vendedor.codTienda, vendedor.tienda, vendedor.id, vendedor.nombre, vendedor.clientes.length, vendedor.contactados, vendedor.contactadosFallidos, vendedor.totalVenta, `${vendedor.minLlamadas} seg.`]);

    }
    this.vendedorService.exportAsExcelFile(excel, 'listadoVendedores');
  }
  async exportarResumen() {
    let excel = [];

    let revisar = [];

    let fechas = this.rangoFecha(this.formFechas.value.fechaInicio, this.formFechas.value.fechaTermino)
    let { fechaInicio, fechaTermino } = fechas

    excel.push(["Resumen Global Vendedores"]);
    excel.push([]);
    excel.push(['Gerente zona', 'tienda', 'Cod. Vendedor', 'Vendedor', 'Rut Cliente', 'Cliente', 'Fecha llamada','Fecha Venta', 'Numero documento', 'SKU', 'Valor', 'Cantidad', 'Total Ventas', 'Duracion llamadas (segundos)']);

    let totalVenta = 0;
    for (let index = 0; index < this.lst.length; index++) {
      const vendedor = this.lst[index];

      totalVenta = 0;

      let clientes = vendedor.clientes;

      for (let index = 0; index < clientes.length; index++) {

        const cliente = clientes[index];

        for (let indice = 0; indice < cliente.registros.length; indice++) {

          const llamada = cliente.registros[indice];
          let fechaLlamada = Number(llamada.idllamada);
          let fecha = new Date(fechaLlamada)
          let total = 0;


          if (fechaInicio < fechaLlamada && fechaLlamada <= fechaTermino && llamada.segundos > 0) {
            if (llamada.segundos > 39) {

              let ventas = vendedor.ventas;
              ventas.filter(venta => {
                let fechab = new Date(venta.Fecha).getTime();
                let valor = 24 * 60 * 60 * 1000 * 7;
  
  
                let fechaI = new Date(fechaLlamada).setHours(0, 0, 0, 0)
                let semana = fechaI + valor;
                let fechaT = new Date(semana).setHours(23, 59, 59, 59);
                if (venta.cliente == cliente.rut && fechaInicio < fechaLlamada && fechaLlamada <= fechaTermino && fechaI < fechab && fechab <= fechaT && vendedor.id === venta.codEmpleado && venta.tipoTransaccion !== "NCE") {
                  total += Math.round(venta.VentaUnit * venta.cantidad)
               
                  revisar.push({
                    subgerente: vendedor.subgerente,
                    tienda: vendedor.tienda,
                    id: vendedor.id,
                    nombreV: vendedor.nombre,
                    rut: cliente.rut,
                    nombreC: cliente.nombre,
                    fechaL: fechaLlamada,
                    fechaDoc: venta.Fecha,
                    doc: venta.numeroDocumento,
                    sku: venta.SKU,
                    venta: Math.round(venta.VentaUnit),
                    cantidad: Math.round(venta.cantidad),
                    total: Math.round(venta.VentaUnit * venta.cantidad),
                    segundos: llamada.segundos
                  });

                }
              })


            }


          }

        }


      }


    }

    for (let index = 0; index < revisar.length - 1; index++) {
      const venta = revisar[index];
      let existe = false;
      for (let i = index + 1; i < revisar.length; i++) {
        const copiaVenta = revisar[i];
        let fechab = new Date(copiaVenta.fechaDoc).getTime();
        let valor = 24 * 60 * 60 * 1000 * 7;
        let fechaI = new Date(venta.fechaL).setHours(0, 0, 0, 0)
        let semana = fechaI + valor;
        let fechaT = new Date(semana).setHours(23, 59, 59, 59);

        if (fechaI < fechab && fechab < fechaT  && venta.doc == copiaVenta.doc && venta.sku == copiaVenta.sku ) {
        
          existe=true
        }
      }
      if(!existe){
        excel.push([
          venta.subgerente,
          venta.tienda,
          venta.id,
          venta.nombreV,
          venta.rut,
          venta.nombreC,
          new Date(venta.fechaL).toLocaleString('es-CL', { timeZone: 'UTC' }),
          new Date(venta.fechaDoc).toLocaleString('es-CL', { timeZone: 'UTC' }),
          venta.doc,
          venta.sku,
          venta.venta,
          venta.cantidad,
          venta.total,
          venta.segundos
        ])
      }
    }

    excel.sort((a,b)=>{
      if(a.doc<b.doc){
        return -1
      }
    })

    await this.vendedorService.exportAsExcelFile(excel, 'ResumenGlobal');

  }

  ordenarLista(tipo, estado) {

    if (estado) {
      estado = false;

      this.lstVendedores.sort((a, b) => {
        if (a[tipo] < b[tipo]) {
          return -1;
        }


        // a must be equal to b
        return 0;
      })

    } else {
      this.lstVendedores.sort((a, b) => {
        if (a[tipo] > b[tipo]) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });
    }


  }

  onDateSelection(date: NgbDate) {

    if (!this.fromDate && !this.toDate) {

      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.formFechas.controls['fechaTermino'].setValue(this.formatter.format(date))
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;

      this.formFechas.controls['fechaInicio'].setValue(this.formatter.format(date))
    }

  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {

    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

}
