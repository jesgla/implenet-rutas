import { Component } from '@angular/core';
import { SiderbarService } from './components/siderbar/siderbar.service';

import { AutentificacionService } from './services/autentificacion/autentificacion.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'implenet';
  currentUser: any;
  constructor(public sidebarservice: SiderbarService,
    private autentificacionService: AutentificacionService) { 
      this.autentificacionService.currentUser.subscribe(x => this.currentUser = x);
    }
  toggleSidebar() {
    this.sidebarservice.setSidebarState(!this.sidebarservice.getSidebarState());
  }
  toggleBackgroundImage() {
    this.sidebarservice.hasBackgroundImage = !this.sidebarservice.hasBackgroundImage;
  }
  getSideBarState() {
    return this.sidebarservice.getSidebarState();
  }

  hideSidebar() {
    this.sidebarservice.setSidebarState(true);
  }

  
}
