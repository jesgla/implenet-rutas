import { BrowserModule, TransferState } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';

import { NgbModule, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { SiderbarComponent } from './components/siderbar/siderbar.component';
import { HomeComponent } from './pages/homePages/home/home.component';
import { AngularSvgIconModule, SvgLoader } from 'angular-svg-icon';
import { Ng2Rut } from 'ng2-rut';
import { NgxSpinnersModule } from 'ngx-spinners';
import { HttpClientModule } from '@angular/common/http';
import { VendedoresComponent } from './pages/vendedorPages/vendedores/vendedores.component';
import { LlamadosComponent } from './pages/llamadoPages/llamados/llamados.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'
import { Traduccion, I18n } from './config/traduccion';

import { PipesModule } from './pipes/pipes.module';
import { TotalVentaComponent } from './components/total-venta/total-venta.component';
import { TotalLlamadasComponent } from './components/total-llamadas/total-llamadas.component';
import { LlamadasComponent } from './pages/llamadoPages/llamadas/llamadas.component';
import { LlamadaTableComponent } from './pages/llamadoPages/components/llamada-table/llamada-table.component';
import { VentasTableComponent } from './pages/llamadoPages/components/ventas-table/ventas-table.component';
import { RutaComponent } from './pages/rutasPages/ruta/ruta.component';
import { LoginComponent } from './pages/loginPages/login/login.component';
import { DetalleVendedorComponent } from './components/modals/detalle-vendedor/detalle-vendedor.component';
import { ResumenVendedorComponent } from './pages/vendedorPages/resumen-vendedor/resumen-vendedor.component';
import { NgxEchartsModule } from 'ngx-echarts';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    SiderbarComponent,
    HomeComponent,
    VendedoresComponent,
    LlamadosComponent,
    TotalVentaComponent,
    TotalLlamadasComponent,
    LlamadasComponent,
    LlamadaTableComponent,
    VentasTableComponent,
    RutaComponent,
    LoginComponent,
    DetalleVendedorComponent,
    ResumenVendedorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    BsDropdownModule.forRoot(),
    PerfectScrollbarModule,
    AngularSvgIconModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,
    Ng2Rut,
    NgxEchartsModule,
    NgxSpinnersModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD_HuwF5F8X8fOSR_1Ai_hFT115caUq4vI'
    }),
    AgmDirectionModule,
  ],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: Traduccion },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
