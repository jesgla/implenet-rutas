import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SiderbarService {
  toggled = true;
  _hasBackgroundImage = true;
  menus = [
    {
      title: 'General',
      type: 'header'
    },
    {
      title: 'CRM',
      icon: './assets/svg/employees.svg',
      svg: './assets/svg/down-arrow.svg',
      active: false,
      type: 'dropdown',
      badge: {
        text: 'New ',
        class: 'badge-warning'
      },
      submenus: [
        {
          title: 'Vendedores',
          ruta:'/vendedores'
  
        },
        {
          title: 'Llamados',
          ruta:'/llamados'
        },
        {
          title: 'Rutas',
          ruta:'/rutas'
        },
        /* {
          title: 'Plataforma de Chat',
          ruta:'/chat'
        }, */
      ]
    },
    
  ];
  constructor() { }

  toggle() {
    this.toggled = ! this.toggled;
  }

  getSidebarState() {
    return this.toggled;
  }

  setSidebarState(state: boolean) {
    this.toggled = state;
  }

  getMenuList() {
    return this.menus;
  }

  get hasBackgroundImage() {
    return this._hasBackgroundImage;
  }

  set hasBackgroundImage(hasBackgroundImage) {
    this._hasBackgroundImage = hasBackgroundImage;
  }
}
