import { TestBed } from '@angular/core/testing';

import { SiderbarService } from './siderbar.service';

describe('SiderbarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SiderbarService = TestBed.get(SiderbarService);
    expect(service).toBeTruthy();
  });
});
