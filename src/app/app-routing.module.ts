import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/homePages/home/home.component';
import { VendedoresComponent } from './pages/vendedorPages/vendedores/vendedores.component';
import { LlamadosComponent } from './pages/llamadoPages/llamados/llamados.component';
import { LlamadasComponent } from './pages/llamadoPages/llamadas/llamadas.component';
import { RutaComponent } from './pages/rutasPages/ruta/ruta.component';
import { AuthGuardGuard } from './guard/auth-guard.guard';
import { LoginComponent } from './pages/loginPages/login/login.component';
import { ResumenVendedorComponent } from './pages/vendedorPages/resumen-vendedor/resumen-vendedor.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardGuard] },
  { path: '', component: HomeComponent, canActivate: [AuthGuardGuard] },
  { path: 'vendedores', component: VendedoresComponent, canActivate: [AuthGuardGuard] },
  { path: 'llamadas', component: LlamadasComponent, canActivate: [AuthGuardGuard] },
  { path: 'llamados', component: LlamadosComponent, canActivate: [AuthGuardGuard] },
  { path: 'rutas', component: RutaComponent, canActivate: [AuthGuardGuard] },
  { path: 'resumenVendedor', component: ResumenVendedorComponent, canActivate: [AuthGuardGuard] },


  { path: '**', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
