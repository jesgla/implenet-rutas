import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutentificacionService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private httpClient: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  async login(usuario) {
    let user = {
      user: usuario.strUsuario,
      pss: 'auth-token'
    }
    let consulta = null;
    const url = `https://b2b-api.implementos.cl/api/movil/loginUsuario`;
    consulta = await this.httpClient.post<any>(url, usuario).toPromise();
 
    if (consulta.length > 0) {
      await localStorage.setItem('usuario', JSON.stringify(consulta[0]));
      await localStorage.setItem('currentUser', JSON.stringify(user));
     
      await this.currentUserSubject.next(user);
      return consulta[0];
    } 
  
  }

  async logout() {
    // remove user from local storage and set current user to null
    await localStorage.removeItem('usuario');
    await localStorage.removeItem('currentUser');
    await this.currentUserSubject.next(null);
  }
}
